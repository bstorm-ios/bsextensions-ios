//
//  URL+Extension.swift
//

import Foundation

public extension URL {
    
    func parms() -> [AnyHashable: Any] {
        let components = URLComponents(url: self, resolvingAgainstBaseURL: false)
        var parms: [AnyHashable: Any] = [:]
        
        if let queryItems = components?.queryItems {
            for item in queryItems {
                parms[item.name] = item.value
            }
        }
        
        return parms
    }
}
