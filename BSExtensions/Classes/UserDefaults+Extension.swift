//
//  UserDefaults+Extension.swift
//  ios-skeleton
//
//  Created by Nemanja Markicevic on 2/6/17.
//  Copyright © 2017 Nemanja Markicevic. All rights reserved.
//

import Foundation

public extension UserDefaults {
  
    enum UserDefaultsTypes: String {
        case type = "Type"
    }

    static let didAskForPushNotificationPermission = "UserDefaultsidAskForPushNotificationPermission"
    
    class var type: UserDefaults? {
        return UserDefaults(suiteName: UserDefaultsTypes.type.rawValue)
    }
    
    var didAskForPushNotificationPermission: Bool {
        get {
            return bool(forKey: UserDefaults.didAskForPushNotificationPermission)
        }
        
        set(value) {
            set(value, forKey: UserDefaults.didAskForPushNotificationPermission)
        }
    }
}
