//
//  UIAlertController+Extension.swift
//

import UIKit

public extension UIAlertController {
    
    static let titleAtributtes: [String : AnyObject] = [NSForegroundColorAttributeName: UIColor.black,
                                                        NSFontAttributeName: UIFont.systemFont(ofSize: 17)]
    static let messageAtributtes: [String : AnyObject] = [NSForegroundColorAttributeName: UIColor.black,
                                                          NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
    
    convenience init(photoTakenHandler: ((UIAlertAction) -> Swift.Void)? = nil, photoChosenHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let cameraAvailable = UIImagePickerController.isSourceTypeAvailable(.camera)
        let photoLibraryAvailable = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        
        if cameraAvailable || photoLibraryAvailable {
            self.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            if cameraAvailable && photoTakenHandler != nil {
                addAction(UIAlertAction(title: "Take photo".localized, style: .default, handler: photoTakenHandler))
            }
            
            if photoLibraryAvailable && photoChosenHandler != nil {
                addAction(UIAlertAction(title: "Choose photo".localized, style: .default, handler: photoChosenHandler))
            }
            
            addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        } else {
            self.init(message:"Camera and Photo Library are not available".localized)
        }
    }
    
//    convenience init(title: String = "Information".localized,
//                     message: String,
//                     alertStyle: UIAlertControllerStyle = .alert,
//                     defaultActionTitle: String = "OK".localized,
//                     defaultActionStyle: UIAlertActionStyle = .default,
//                     defaultActionHandler: ((UIAlertAction) -> Void)? = nil) {
//        
//        self.init(title: nil, message: nil, preferredStyle: alertStyle)
//        view.tintColor = UIColor.black
//        
//        let attributedTitle = NSMutableAttributedString(string: title, attributes: UIAlertController.titleAtributtes)
//        let attributedMessage = NSMutableAttributedString(string: message, attributes: UIAlertController.messageAtributtes)
//        
//        self.setValue(attributedTitle, forKey: "attributedTitle")
//        self.setValue(attributedMessage, forKey: "attributedMessage")
//        
//        addAction(UIAlertAction(title: defaultActionTitle, style: defaultActionStyle, handler: defaultActionHandler))
//    }
    
    /// Informational AlertController with OK dismiss button
    ///
    /// - Parameters:
    ///   - title: Optional -> default = Information
    ///   - message: Alert message
    convenience init(title: String = "Information".localized, message: String) {
        self.init(title: title, message: message, preferredStyle: .alert)
        addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    }
}
