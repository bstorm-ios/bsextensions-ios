//
//  Storyboard+Extension.swift
//

import UIKit

public extension UIStoryboard {
    
    enum StoryboardType: String {
        case menu
        case main
        case walkthrough
        case photoSelection
        case login
        
        var filename: String {
            return rawValue.capitalizeFirstLetter
        }
    }
    
    convenience init(type storyboard: StoryboardType, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }
    
    func presentInitialViewController(on viewController: UIViewController? = UIViewController.top,
                                      withTransitionStyle transitionStyle: UIModalTransitionStyle = .coverVertical,
                                      completion: (() -> Void)? = nil) {
        
        if let vc = instantiateInitialViewController() {
            vc.modalTransitionStyle = transitionStyle
            viewController?.present(vc, animated: true, completion: completion)
        }
    }
}
