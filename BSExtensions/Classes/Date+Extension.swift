//
//  Date+Extension.swift
//  ios-skeleton
//
//  Created by Nemanja Markicevic on 1/30/17.
//  Copyright © 2017 Nemanja Markicevic. All rights reserved.
//

import Foundation

public extension Date {
    
    static let min = 60
    static let hour = min * 60
    static let day = hour * 24
    static let week = day * 7
    static let mounth = week * 4
    static let year = mounth * 12
    
    static func fromString(_ date: String) -> String {
        
        let date = Date(timeIntervalSince1970: TimeInterval(date) ?? TimeInterval())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm MM.dd.yyyy"
        return dateFormatter.string(from: date)
    }
    
    static func fromDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm MM.dd.yyyy"
        return dateFormatter.string(from: date)
    }
    
    static func from(_ string: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm MM.dd.yyyy"
        
        return dateFormatter.date(from: string)!
    }
    
    static func timePass(since date: Date) -> String {
        let interval = Date.timeIntervalSince(Date())
        let sec = Int(interval(date))
        
        var time = ""
        switch sec {
        case _ where sec > year:    time = "\(sec / year)y"
        case _ where sec > mounth:  time = "\(sec / mounth)m"
        case _ where sec > week:    time = "\(sec / week)w"
        case _ where sec > day:     time = "\(sec / day)d"
        case _ where sec > hour:    time = "\(sec / hour)h"
        case _ where sec > min:     time = "\(sec / min)m"
        default:                    time = "\(sec)s"
        }
        return time
    }
}
