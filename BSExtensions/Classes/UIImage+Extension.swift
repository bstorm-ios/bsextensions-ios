//
//  UIImage+Extension.swift
//

import UIKit

public extension UIImage {
    
    func scaleTo(size: CGSize) -> UIImage {
        var image = self
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
}
