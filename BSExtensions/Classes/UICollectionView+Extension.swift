//
//  UICollectionView+Extension.swift
//

import Foundation

public extension UICollectionView {
    
    func deselectAllItems(animated: Bool = true) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
}
