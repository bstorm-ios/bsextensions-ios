//
//  UIApplication+Extension.swift
//

import UIKit

public extension UIApplication {
    
    func changeRoot(to newRoot: UIViewController?, animated: Bool = true) {
        if let window = delegate?.window, let newRoot = newRoot {
            window?.rootViewController?.present(newRoot, animated: animated) {
                window?.rootViewController = newRoot
                window?.makeKeyAndVisible()
            }
        }
    }
    
    func change(storyboard: UIStoryboard) {
        changeRoot(to: storyboard.instantiateInitialViewController())
    }
}
