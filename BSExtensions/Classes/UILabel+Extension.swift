//
//  UILabel+Extension.swift
//

import UIKit

public extension UILabel {
    
    func set(htmlAttributedString: String) {
        let string = htmlAttributedString.appending("<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize);}</style>")
        
        guard let data = string.data(using: .utf8) else {
            text = string
            return
        }
        
        do {
            let attributedTextOptions = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
            attributedText = try NSAttributedString(data: data, options: attributedTextOptions, documentAttributes: nil)
        } catch {
            text = string
        }
    }
    
    func set(shadowedText: String) {
        let shadow = NSShadow()
        shadow.shadowColor = UIColor.black.withAlphaComponent(0.5)
        shadow.shadowOffset = CGSize(width: 0, height: 0)
        shadow.shadowBlurRadius = 5
        attributedText = NSAttributedString(string: shadowedText, attributes: [NSShadowAttributeName: shadow, NSFontAttributeName: font])
    }
}
