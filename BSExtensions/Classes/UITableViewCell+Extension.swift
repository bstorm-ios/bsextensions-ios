//
//  UITableViewCell+Extension.swift
//

import Foundation

public extension UITableViewCell {
    
    static var cellIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    var indexPath: IndexPath? {
        guard let tableView = superview?.superview as? UITableView else { return nil }
        return (tableView.indexPath(for: self))
    }
}
