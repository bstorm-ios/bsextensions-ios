//
//  String+Extension.swift
//

import Foundation

public extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var count: Int {
        return characters.count
    }
    
    var url: URL? {
        return URL(string: self)
    }
    
    var capitalizeFirstLetter: String {
        let firstIndex = index(startIndex, offsetBy: 1)
        return substring(to: firstIndex).capitalized + substring(from: firstIndex)
    }
    
    var capitalizeOnlyFirstLetter: String {
        let firstIndex = index(startIndex, offsetBy: 1)
        return substring(to: firstIndex).capitalized + substring(from: firstIndex).lowercased()
    }
    
    func formatParametars(_ params: [String]) -> String {
        return String(format: self, arguments: params)
    }
}
