//
//  UIViewController+Extension.swift
//

import UIKit

extension UIViewController: UIViewControllerLoading {
    
    public static var top: UIViewController? {
        return topViewController()
    }
    
    static var root: UIViewController? {
        return UIApplication.shared.delegate?.window??.rootViewController
    }
    
    static func topViewController(from viewController: UIViewController? = UIViewController.root) -> UIViewController? {
        if let tabBarViewController = viewController as? UITabBarController {
            return topViewController(from: tabBarViewController.selectedViewController)
        } else if let navigationController = viewController as? UINavigationController {
            return topViewController(from: navigationController.visibleViewController)
        } else if let presentedViewController = viewController?.presentedViewController {
            return topViewController(from: presentedViewController)
        } else {
            return viewController
        }
    }
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

/// Protocol to be extended with implementations
public protocol UIViewControllerLoading {}

/// Protocol implementation
public extension UIViewControllerLoading where Self : UIViewController {
    
    /// Instantiate view controller from storyboard
    ///
    /// - Parameter storyboard: Storyboard that the ViewController is on
    /// - Returns: ViewController as the type that was called from
    static func instantiate(from storyboard: UIStoryboard.StoryboardType = .main) -> Self? {
        let storyboard = UIStoryboard(type: storyboard)
        return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as? Self
    }
}
