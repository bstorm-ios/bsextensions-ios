//
//  UIImageView+Extension.swift
//

import UIKit

public extension UIImageView {
    
    func setRounded() {
        layer.masksToBounds = false
        layer.cornerRadius = frame.size.height / 2
        clipsToBounds = true
    }
}
