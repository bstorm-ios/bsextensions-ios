//
//  UICollectionView+Extension.swift
//

import Foundation

public extension UICollectionViewCell {
    
    static var cellIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}
