//
//  UIView+IBExtension.swift
//

import UIKit

/// Protocol to be extended with implementations
public protocol UIViewLoading {}

extension UIView: UIViewLoading {
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

/// Protocol implementation
public extension UIViewLoading where Self : UIView {
    /**
     Creates a new instance of the class on which this method is invoked,
     instantiated from a nib of the given name. If no nib name is given
     then a nib with the name of the class is used.
     
     - parameter nibNameOrNil: The name of the nib to instantiate from, or
     nil to indicate the nib with the name of the class should be used.
     
     - returns: A new instance of the class, loaded from a nib.
     */
    static func loadFromNib(nibNameOrNil: String? = nil) -> Self? {
        let nibName = nibNameOrNil ?? self.className
        return Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?.first as? Self
    }
    
    private static var className: String {
        return "\(self)"
    }
}
